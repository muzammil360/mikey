import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

import time
import numpy as np
import matplotlib.pyplot as plt

from DataReader import DataReader 



# config
PIPELINE = '''
	filesrc location=/home/muzammil/freeform/mikey-multani/flir_GRAY16_LE.raw blocksize=38400 do-timestamp=false ! video/x-raw, format=GRAY16_LE, framerate=9/1, width=160, height=120 ! identity silent=false sleep-time=111111 
	! appsink caps="video/x-raw, format=GRAY16_LE, framerate=9/1, width=160, height=120"
	'''
TMP_LOWER_LIM= 20.0
TMP_UPPER_LIM= 40.0
NUM_COLORBAR_TICKS = 5
MAINLOOP_PAUSE_SEC = 0.1
COLORMAP_NAME = 'Spectral'

def raw2temp(input_data):
    '''
    converts raw data to temperature in degree centigrade
    '''
    output = (input_data/100) - 273.15
    return output

def main():
    pipeline = PIPELINE
    tmp_lower_lim = TMP_LOWER_LIM
    tmp_upper_lim = TMP_UPPER_LIM
    num_colorbar_ticks = NUM_COLORBAR_TICKS
    mainloop_pause_sec = MAINLOOP_PAUSE_SEC
    colormap_name = COLORMAP_NAME

    is_first_time = True

    # make reader 
    reader = DataReader(pipeline)
    reader.start()

    while(True):
        buffer = reader.readBuffer()
        if buffer is not None:

            # convert to temperature
            tmp_data = raw2temp(buffer)
            

            # scale [tmp_lower_lim, tmp_upper_lim] to [0, 255]
            color_map_data = np.interp(tmp_data, [tmp_lower_lim, tmp_upper_lim], [0, 255])

            # display colormap
            color_map_data = color_map_data.astype(np.uint8)    # convert to unit8
            color_map_data[0][0]=0                  # hack to make sure lower limit is well defined
            color_map_data[0][1]=255                # hack to make sure upper limit is well defined


            ticklist = np.linspace(0, 255, num_colorbar_ticks).tolist()         # tick position
            ticklist_labels = list(map(str,np.linspace(tmp_lower_lim, tmp_upper_lim, num_colorbar_ticks).tolist()))

            if is_first_time == True:
                # Lets be efficient. we make the plots only once, then we just update data everytime
                image_axes = plt.imshow(color_map_data, cmap=colormap_name) # show image
                cbar = plt.colorbar(image_axes, ticks=ticklist) # add colorbar with tick positions
                cbar.ax.set_yticklabels(ticklist_labels)        # define tick labels
                is_first_time = False
            else:
                image_axes.set_data(color_map_data)

            plt.draw()
            plt.pause(mainloop_pause_sec)

        




    


    pass 


if __name__ == '__main__':
    
    try:
        main()
    except KeyboardInterrupt as exp:
        plt.close('all')