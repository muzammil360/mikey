# Introduction
In this demo, we read FLIR thermal data using gstreamer pipeline and display it. 


# Docker
# build
```
docker build -t flir-gstreamer .
```

## development 
**WARNING**: following code is for dev purpose only. use if you know what you are doing
```
SRC=/home/muzammil
DEST=/home/muzammil


xhost +
docker run -itd \
--name pygst.docker \
 -v ${SRC}:${DEST} \
 -e DISPLAY=$DISPLAY \
-v /tmp/.X11-unix:/tmp/.X11-unix \
 muzammil360/python-gstreamer:latest bash

# run following once you are one with docker 
xhost -
```
# How to run 
```
xhost +
docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix flir-gstreamer
xhost - 
```

**WARNING**: before you can run above, you will need to log into gitlab docker registry
```
docker login registry.gitlab.com -u <GITLAB_USERNAME> -p <GITLAB_PASSWORD>
```

# Module list
- 1-temperature-display: visualizes Flir temperatue data

# Contact
For questions and comments, find me here: muzammil360@gmail.com
