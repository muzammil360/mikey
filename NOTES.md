# Sample Gstreamer pipeline 
### raw recording
GST_DEBUG=2 gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,format=GRAY16_LE ! videoconvert !  filesink location=flir_GRAY16_LE.raw

### raw recording playback 
GST_DEBUG=3 gst-launch-1.0 filesrc location=/home/muzammil/freeform/mikey-multani/flir_GRAY16_LE.raw blocksize=38400 do-timestamp=true ! video/x-raw, format=GRAY16_LE, framerate=9/1, width=160, height=120 ! videoconvert ! identity silent=false sleep-time=111111 !  xvimagesink
**WARNING**: above pipeline `videoconvert` converts 16 bit data to 8 bit for display