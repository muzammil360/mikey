FROM muzammil360/python-gstreamer:latest

COPY 1-temperature-display 1-temperature-display

CMD ["python3", "1-temperature-display/main.py"]